import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });


  const fighterImg = createFighterImage(fighter);
  const fighterUser = createElement({
    tagName: 'div',
    className: 'fighter-preview__user',
  });

  const { name, health, attack, defense } = fighter;

  fighterUser.innerHTML = `
    <h1 style="color: goldenrod;">${name}</h1>
    <p class='arena___fighter-name'>Helth: ${health}</p>
    <p class='arena___fighter-name'>Attack: ${attack}</p>
    <p class='arena___fighter-name'>Defense: ${defense}</p>`;

  fighterElement.append( fighterImg, fighterUser );

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
