import {showModal} from './modal';

export function showWinnerModal(fighter) {

  const name = `${fighter.name} is the Winner!`;
  const blockWinner = document.createElement('div');
  blockWinner.classList.add('modal-winner__user');

  //blockWinner.innerText = `${fighter.name} has bean Win!`;

  const result = {
    title: name,
    bodyElement: blockWinner,
    onClose: () => {
      window.location.reload()
    }
  };
  showModal(result);
}